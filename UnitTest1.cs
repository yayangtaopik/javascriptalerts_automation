using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace javascriptpopup;

public class Tests
{
    String test_url = "http://the-internet.herokuapp.com/javascript_alerts";
    IWebDriver driver;

    [SetUp]
    public void Setup()
    {
    }

    public void start_Browser()
        {
            // Local Selenium WebDriver
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }
    
    public void test_jsAlert(){
        String button_xpath = "//button[.='Click for JS Alert']";
        var expectedAlertText = "I am a JS Alert";

        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        driver.Url = test_url;
 
        /* IWebElement alertButton = driver.FindElement(By.XPath(button_xpath)); */
 
         IWebElement alertButton = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(button_xpath)));
        alertButton.Click();
 
        var alert_win = driver.SwitchTo().Alert();
        Assert.AreEqual(expectedAlertText, alert_win.Text);
 
        alert_win.Accept();
 
            /* IWebElement clickResult = driver.FindElement(By.Id("result")); */
 
        var clickResult = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id("result")));
 
        Console.WriteLine(clickResult.Text);
 
            if (clickResult.Text == "You successfuly clicked an alert")
            {
                Console.WriteLine("Alert Test Successful");
            }
        
    }
    
    public void test_jsConfirm(){
         String button_css_selector = "button[onclick='jsConfirm()']";
            var expectedAlertText = "I am a JS Confirm";
 
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Url = test_url;
 
            IWebElement confirmButton = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(button_css_selector)));
 
            confirmButton.Click();
 
            var confirm_win = driver.SwitchTo().Alert();
            confirm_win.Accept();
 
            IWebElement clickResult = driver.FindElement(By.Id("result"));
            Console.WriteLine(clickResult.Text);
 
            if (clickResult.Text == "You clicked: Ok")
            {
                Console.WriteLine("Confirm Test Successful");
            }
    }

    public void test_jsDismiss(){
        String button_css_selector = "button[onclick='jsConfirm()']";
            var expectedAlertText = "I am a JS Confirm";
 
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Url = test_url;
 
            IWebElement confirmButton = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(button_css_selector)));
 
            confirmButton.Click();
 
            var confirm_win = driver.SwitchTo().Alert();
            confirm_win.Dismiss();
 
            IWebElement clickResult = driver.FindElement(By.Id("result"));
            Console.WriteLine(clickResult.Text);
 
            if (clickResult.Text == "You clicked: Cancel")
            {
                Console.WriteLine("Dismiss Test Successful");
            }
    }
    public void test_jsPrompt_sendTextAlert(){
        String button_css_selector = "button[onclick='jsPrompt()']";
            var expectedAlertText = "I am a JS prompt";
 
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Url = test_url;
 
            IWebElement confirmButton = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(button_css_selector)));
            confirmButton.Click();
 
            var alert_win = driver.SwitchTo().Alert();
            alert_win.SendKeys("This is a test alert message");
            alert_win.Accept();
 
            IWebElement clickResult = driver.FindElement(By.Id("result"));
            Console.WriteLine(clickResult.Text);
 
            if (clickResult.Text == "You entered: This is a test alert message")
            {
                Console.WriteLine("Send Text Alert Test Successful");
            }
    }

    [Test]
    public void TestAlert()
    {   
        //js alert
        Console.WriteLine("Test js Alert");
        start_Browser();
        test_jsAlert();
        test_jsConfirm();
        test_jsDismiss();
        test_jsPrompt_sendTextAlert();
        
    }
}